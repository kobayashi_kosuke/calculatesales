package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品別集計ファイル
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "支店定義ファイルが存在しません";
	private static final String GOODSFILE_NOT_EXIST = "商品定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "支店定義ファイルのフォーマットが不正です";
	private static final String GOODSFILE_INVALID_FORMAT = "商品定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_SERIAL = "売上ファイル名が連番になっていません";
	private static final String SALES_OVER = "合計金額が10桁を超えました";
	private static final String BRANCH_INVALID_CODE = "の支店コードが不正です";
	private static final String GOODS_INVALID_CODE = "の商品コードが不正です";
	private static final String SALES_INVALID_FORMAT = "のフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		//コマンドライン引数が1つ設定されているかを確認
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> goodsNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> goodsSales = new HashMap<>();
		//引数に当てはまるよう正規表現を用いた変数を宣言
		String branchCodeMatches = "[0-9]{3}";
		String goodsCodeMatches = "[0-9][a-z][A-Z]{8}";

		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, branchCodeMatches, FILE_NOT_EXIST, FILE_INVALID_FORMAT )) {
			return;
		}
		// 商品定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_COMMODITY_LST, goodsNames, goodsSales, goodsCodeMatches, GOODSFILE_NOT_EXIST, GOODSFILE_INVALID_FORMAT)) {
			return;
		}


		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		ArrayList<File> rcdFiles = new ArrayList<>();

		// 正規表現を用いて該当したファイルをArrayListに格納する
		// ファイルであるかどうかも確認する
		for (int i = 0; i < files.length; i++) {
			if (files[i].isFile() && files[i].getName().matches("[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		//順番に格納したファイル名の数字の差から連番であるかどうかを確認する
		Collections.sort(rcdFiles);
		for (int i = 0; i < rcdFiles.size() - 1; i++) {

			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if (latter - former != 1) {
				System.out.println(FILE_NOT_SERIAL);
				return;
			}
		}
		BufferedReader br = null;
		//ファイルを一行ずつ読み取り、リストに格納する
		for (int i = 0; i < rcdFiles.size(); i++) {

			//FileクラスのArrayListから格納されたファイルを取得し読み込む
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				String line;
				//キャスト時にString型とLong型を分けるためArrayList変数を宣言する
				ArrayList<String> branchCode = new ArrayList<>();
				while ((line = br.readLine()) != null) {
					branchCode.add(line);
				}
				//売上ファイルのフォーマットが3行であることを確認
				if(branchCode.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + SALES_INVALID_FORMAT);
					return;
				}
				//支店定義Mapに支店コードがKeyとして存在するかを確認
				if(!branchSales.containsKey(branchCode.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + BRANCH_INVALID_CODE);
					return;
				}
				//商品定義Mapに商品コードがKeyとして存在するかを確認
				if(!goodsSales.containsKey(branchCode.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + GOODS_INVALID_CODE);
					return;
				}
				//正規表現を用いて売上が数字であることを確認
				if(!branchCode.get(2).matches("[0-9]{1,10}")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				//リストから要素を取得し、売上である2番目の要素をLong型にキャスト
				//0番目のString型の要素を支店定義MapのKeyとして取得
				//1番目のString型の要素を商品定義Mapのkeyとして取得
				//Long変数amountを宣言し、MapのValueと売上を合計し代入
				long sales = Long.parseLong(branchCode.get(2));
				long goodsAmount = goodsSales.get(branchCode.get(1)) + sales;
				Long amount = branchSales.get(branchCode.get(0)) + sales;


				//売上金額が10桁以内になっているかを確認
				if(amount >= 10000000000L || goodsAmount >= 10000000000L) {
					System.out.println(SALES_OVER);
					return;
				}

				//代入した金額を再び適宜Mapに格納
				branchSales.put(branchCode.get(0), amount);
				goodsSales.put(branchCode.get(1), goodsAmount);

			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if (br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		// 商品別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, goodsNames, goodsSales)) {
			return;
		}
	}
	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names,
			Map<String, Long> sales, String codeMatches, String notExist, String invalidFormat) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			//existsメソッドを用いてファイルの存在を確認
			if(!file.exists()) {
				System.out.println(notExist);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {
				// 読み込み処理1-2
				String items[] = line.split(",");

				//カンマで区切ったitemsの要素数が2であること、支店コードが3桁の整数であること
				//商品コードが有効であることをString matchを用いて確認
				if(items.length != 2 || !items[0].matches(codeMatches)) {
					System.out.println(invalidFormat);
					return false;
				}
				//適宜Mapに格納
					names.put(items[0], items[1]);
					sales.put(items[0], 0L);
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names,
			Map<String, Long> sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;
		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			//拡張for文でString変数branchKeyにkeyを代入、フォーマットに当てはまるよう書き込み、改行
			for (String branchKey : sales.keySet()) {
				bw.write(branchKey + "," + names.get(branchKey) + "," + sales.get(branchKey));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
